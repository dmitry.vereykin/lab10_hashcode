import java.util.Scanner;

/**
 * Created by Dmitry Vereykin on 11/24/2015.
 */
public class HashChk {

    public static void main(String[] args) {
        String str;
        int hash;
        Scanner input = new Scanner(System.in);

        while (true) {
            System.out.print("Enter a string or \"stop\" to exit: ");
            //input.nextLine();
            str = input.nextLine();
                if (str.equalsIgnoreCase("stop")) {
                    System.out.println("Exiting...");
                    break;
                } else {
                    hash = str.hashCode();
                    System.out.println("Hashcode of " + "\"" + str + "\"" + " is " + hash + "\n");
                }
        }
    }
}
